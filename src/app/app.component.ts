import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { AuthService } from "./services/auth.service";

declare const $;
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  @ViewChild("sidenav", { static: false }) sidenav: ElementRef;

  title = "heros";

  constructor(private _authService: AuthService) {}

  ngOnInit() {}

  logout() {
    this._authService.logout();
  }

  showDialog() {
    let modal_t = document.getElementById("modal_1");
    modal_t.classList.remove("hhidden");
    modal_t.classList.add("sshow");
  }
}
