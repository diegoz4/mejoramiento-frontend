import { NgModule, ModuleWithComponentFactories } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//importaciones de la información del plan

import { PlansComponent } from "./pages/infoPlan/plans/plans.component";
import { ObjectivesComponent } from "./pages/infoPlan/objectives/objectives.component";
import { IndicatorsComponent } from "./pages/infoPlan/indicators/indicators.component";
import { DescripIndicatorComponent } from "./pages/infoPlan/descrip-indicator/descrip-indicator.component";

//importaciones de administración

import { FacultiesComponent } from "./pages/carpAdmin/faculties/faculties.component";
import { ItemIndicatorComponent } from "./pages/carpAdmin/item-indicator/item-indicator.component";
import { UsersComponent } from "./pages/carpAdmin/users/users.component";
import { PermissionsComponent } from './pages/carpAdmin/permissions/permissions.component';

//importaciones de dashboard
import { DashboardComponent } from "./pages/carpDashboard/dashboard/dashboard.component";
import { DasObjectiveComponent } from "./pages/carpDashboard/das-objective/das-objective.component";
import { DasIndicatorComponent } from "./pages/carpDashboard/das-indicator/das-indicator.component";
import { DasGraphComponent } from "./pages/carpDashboard/das-graph/das-graph.component";
import { InfoActivityComponent } from "./pages/carpDashboard/info-activity/info-activity.component";

//importaciones de la carpeta de actividades

import { ListPlanComponent } from "./pages/carpActivities/list-plan/list-plan.component";
import { ListObjectiveComponent } from "./pages/carpActivities/list-objective/list-objective.component";
import { ListIndicatorComponent } from "./pages/carpActivities/list-indicator/list-indicator.component";
import { AddActivityComponent } from "./pages/carpActivities/add-activity/add-activity.component";
import { InfoActivitiesComponent } from "./pages/carpActivities/info-activities/info-activities.component";

//Importaciones de la carpeta mediciones

import { LstPlansComponent } from "./pages/carpMesumerent/lst-plans/lst-plans.component";
import { LstObjetiveComponent } from "./pages/carpMesumerent/lst-objetive/lst-objetive.component";
import { LstIndicatorComponent } from "./pages/carpMesumerent/lst-indicator/lst-indicator.component";
import { AddMesurementComponent } from "./pages/carpMesumerent/add-mesurement/add-mesurement.component";

//Importaciones de la carpeta de reporte

import { ReportPlanComponent } from "./pages/carpReports/report-plan/report-plan.component";
import { ReportObjectiveComponent } from "./pages/carpReports/report-objective/report-objective.component";

import { from } from "rxjs";
const routes: Routes = [
  //rutas de la lista de planes con su información
  { path: "infoPlanes", component: PlansComponent },
  { path: "infoObjetivos/:id", component: ObjectivesComponent },
  { path: "infoIndicadores/:id", component: IndicatorsComponent },
  { path: "Indicador/:id", component: DescripIndicatorComponent },

  //rutas del administador de la app
  { path: "facultades", component: FacultiesComponent },
  { path: "Item-indicador", component: ItemIndicatorComponent },
  { path: "users", component: UsersComponent },
  { path: "permissions", component: PermissionsComponent},

  //rutas del dashboard
  { path: "dashboard", component: DashboardComponent },
  { path: "dashObjetive/:id", component: DasObjectiveComponent },
  { path: "dashIndicator/:id", component: DasIndicatorComponent },
  { path: "Graph/:id", component: DasGraphComponent },
  { path: "Activity/:id", component: InfoActivityComponent },

  //rutas para las actividades
  { path: "ActPlans", component: ListPlanComponent },
  { path: "ActObjective/:id", component: ListObjectiveComponent },
  { path: "ActIndicator/:id", component: ListIndicatorComponent },
  { path: "addActivity/:id", component: AddActivityComponent },
  { path: "infoActivity/:id", component: InfoActivitiesComponent },

  //rutas para las mediciones
  { path: "MedPlans", component: LstPlansComponent },
  { path: "MedObjective/:id", component: LstObjetiveComponent },
  { path: "MedIndicator/:id", component: LstIndicatorComponent },
  { path: "addMesurement/:id", component: AddMesurementComponent },

  { path: "dddd", component: AddMesurementComponent },
  
  //rutas para los reportes
  { path: "reporPlan", component: ReportPlanComponent },
  { path: "reporObjective/:id", component: ReportObjectiveComponent },

  //----------------------------------------------------------
  { path: "**", pathMatch: "full", redirectTo: "dashboard" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
