import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "app-list-indicator",
  templateUrl: "./list-indicator.component.html",
  styleUrls: ["./list-indicator.component.css"],
})
export class ListIndicatorComponent implements OnInit {
  dataHeaders: any;
  idObjective: number;
  currentObjetive: any = { name: "" };
  forFilter: any;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idObjective = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "objective", value: this.idObjective };
  }
  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.currentObjetive = await this.dataService
      .getDataItem("objectives/", this.idObjective)
      .then((rs) => {
        return rs;
      });
    if (this.currentObjetive != null) {
      this.cambio = false;
    }
  }
  test(event) {
    this.router.navigate(["addActivity", event.id]);
  }
}
