import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import { DynamicTableComponent } from "src/app/components/dynamic-table/dynamic-table.component";

import { NgForm } from "@angular/forms";
import Swal from "sweetalert2";

const swal = require("sweetalert2");
@Component({
  selector: "app-add-activity",
  templateUrl: "./add-activity.component.html",
  styleUrls: ["./add-activity.component.css"],
})
export class AddActivityComponent implements OnInit {
  @ViewChild("tbActivities", { static: false })
  dynamicTableActivity: DynamicTableComponent;

  dataHeaders: any;
  idIndicator: number;
  currentIndicator: any = { name: "" };
  forFilter: any;

  // variables para agregar datos
  nameActivity: string;
  dateInitial;
  dateFinal;
  estimated: string;
  responsable: string;
  stad: number;
  states: {};
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.dataService.getListItems("states/").then((r) => (this.states = r));
    this.idIndicator = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "indicator", value: this.idIndicator };
  }

  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.currentIndicator = await this.dataService
      .getDataItem("indicators/", this.idIndicator)
      .then((rs) => {
        return rs;
      });
    if (this.currentIndicator != null) {
      this.cambio = false;
    }
  }

  sendData() {
    let json = {
      name: this.nameActivity,
      responsable: this.responsable,
      estimated: this.estimated,
      start_date: this.dateInitial.toISOString().slice(0, 10),
      end_date: this.dateFinal.toISOString().slice(0, 10),
      indicator: this.idIndicator,
      state: this.stad,
    };
    this.dataService.addItem("activities/", json).subscribe((rs) => {
      swal.fire("", "Se agrego Correctamente la Actividad", "success");
      this.updateDateTable();
    });
  }
  updateDateTable() {
    this.dynamicTableActivity.getList();
  }

  algo(value) {
    this.stad = value;
  }
  test(event) {
    this.router.navigate(["infoActivity", event.id]);
  }
}
