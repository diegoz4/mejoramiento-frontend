import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoActivitiesComponent } from './info-activities.component';

describe('InfoActivitiesComponent', () => {
  let component: InfoActivitiesComponent;
  let fixture: ComponentFixture<InfoActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
