import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "app-info-activities",
  templateUrl: "./info-activities.component.html",
  styleUrls: ["./info-activities.component.css"],
})
export class InfoActivitiesComponent implements OnInit {
  dataHeaders: any;
  idActivity: number;
  currentIndicator: any = { name: "" };
  estado: any = { state: "" };
  listEstados: {};
  editar: boolean = false;

  //var Para editar actividad

  name: String;
  responsable: String;
  estimated: string;
  dateInitial;
  dateFinal;
  stad: number;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService
  ) {
    this.infoActivity();
    this.dataService
      .getListItems("states/")
      .then((r) => (this.listEstados = r));
    this.idActivity = this.activatedRouter.snapshot.params.id;
  }

  async ngOnInit() {
    this.infoActivity();
    if (this.currentIndicator != null || this.estado != null) {
      this.cambio = false;
    }
  }

  async infoActivity() {
    this.currentIndicator = await this.dataService
      .getDataItem("activities/", this.idActivity)
      .then((rs) => {
        return rs;
      });
    this.estados();
  }

  estados() {
    this.estado = Object.values(this.listEstados).filter(
      (st) => st["id"] == this.currentIndicator["state"]
    )[0];
  }
  cambioEditar() {
    this.editar = true;
    this.name = this.currentIndicator["name"];
    this.responsable = this.currentIndicator["responsable"];
    this.estimated = this.currentIndicator["estimated"];
    this.dateInitial = new Date(this.currentIndicator["start_date"]);
    this.dateFinal = new Date(this.currentIndicator["end_date"]);
    this.stad = this.currentIndicator["state"];
  }
  UpdateActivity() {
    let json = {
      id: this.idActivity,
      name: this.name,
      responsable: this.responsable,
      estimated: this.estimated,
      start_date: this.dateInitial.toISOString().slice(0, 10),
      end_date: this.dateFinal.toISOString().slice(0, 10),
      indicator: this.currentIndicator["indicator"],
      state: this.stad,
    };
    this.dataService
      .editItem("activities/", this.idActivity, json)
      .subscribe((rs) => {
        this.editar = false;
        this.infoActivity();
      });
  }
  algo(value) {
    this.stad = value;
  }
}
