import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { async } from "rxjs/internal/scheduler/async";
import { RouterLink, Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  dataTable: any;
  dataHeaders: any[];
  cambio: boolean = true;
  constructor(private router: Router) {}

  ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];
    setTimeout(() => {
      this.cambio = false;
    }, 2000);
  }

  test(event) {
    this.router.navigate(["dashObjetive", event.id]);
  }
}
