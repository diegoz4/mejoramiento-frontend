import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "app-info-activity",
  templateUrl: "./info-activity.component.html",
  styleUrls: ["./info-activity.component.css"],
})
export class InfoActivityComponent implements OnInit {
  idActivity: number;
  forFilter: any;
  currentActivity: any = { name: "" };
  dataHeaders: any;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idActivity = this.activatedRouter.snapshot.params.id;
  }

  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.currentActivity = await this.dataService
      .getDataItem("activities/", this.idActivity)
      .then((rs) => {
        return rs;
      });
    if (this.currentActivity != null) {
      this.cambio = false;
    }
  }
}
