import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";

const swal = require("sweetalert2");
@Component({
  selector: "app-das-graph",
  templateUrl: "./das-graph.component.html",
  styleUrls: ["./das-graph.component.css"],
})

// *ngFor="let item of noInicia"
export class DasGraphComponent implements OnInit {
  activities: any;
  listActivity: {};
  noInicia: {};
  cancelada: {};
  enCurso: {};
  ejecutada: {};
  finalizada: {};
  goals: {};
  idIndicator: Number;
  currentIndicator: any = { name: "" };

  already: boolean = false;

  //para la grafica circular

  numNoInicia: number;
  numCancelada: number;
  numEnCurso: number;
  numEjecutada: number;
  numFinalizada: number;

  //para la grafica de mediciones

  listMedicion: any[] = [];
  listMedicionGraf: any[] = [];
  cantMedicion: any[] = [];
  lineaBase: any[] = [];
  metas: any[] = [];

  valorLineaBase: number;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idIndicator = this.activatedRouter.snapshot.params.id;
  }

  async ngOnInit() {
    this.currentIndicator = await this.dataService
      .getDataItem("indicators/", this.idIndicator)
      .then((rs) => {
        return rs;
      });

    this.activities = await this.dataService
      .getListItems("activities/")
      .then((st) => {
        return st;
      });

    //para la grafica de lineas
    this.listMedicion = await Object.values(
      await this.dataService.getListItems("calculated/").then((st) => {
        return st;
      })
    ).filter((v) => v.indicator == this.currentIndicator.id);

    this.goals = await Object.values(
      await this.dataService.getListItems("goals/").then((st) => {
        return st;
      })
    ).filter((v) => v.indicator == this.currentIndicator.id);
    this.conteo();

    if (
      this.currentIndicator != null ||
      this.activities != null ||
      this.listMedicion != null ||
      this.goals != null
    ) {
      this.cambio = false;
    }
  }

  ngAfterViewInit() {}

  info(event) {
    this.router.navigate(["Activity", event]);
  }

  //--para la grafica circular
  conteo() {
    this.noInicia = Object.values(this.activities).filter(
      (v) => v["indicator"] == this.currentIndicator.id && v["state"] == 1
    );

    this.enCurso = Object.values(this.activities).filter(
      (v) => v["indicator"] == this.currentIndicator.id && v["state"] == 2
    );

    this.cancelada = Object.values(this.activities).filter(
      (v) => v["indicator"] == this.currentIndicator.id && v["state"] == 3
    );

    this.finalizada = Object.values(this.activities).filter(
      (v) => v["indicator"] == this.currentIndicator.id && v["state"] == 4
    );

    this.ejecutada = Object.values(this.activities).filter(
      (v) => v["indicator"] == this.currentIndicator.id && v["state"] == 5
    );

    this.numNoInicia = Object.values(this.noInicia).length;
    this.numEnCurso = Object.values(this.enCurso).length;
    this.numCancelada = Object.values(this.cancelada).length;
    this.numFinalizada = Object.values(this.finalizada).length;
    this.numEjecutada = Object.values(this.ejecutada).length;

    this.already = true;

    if (this.listMedicion) {
      for (let key in this.listMedicion) {
        this.listMedicionGraf.push(this.listMedicion[key].value);

        this.lineaBase.push(this.currentIndicator.base_line);
      }
    }

    if (this.goals) {
      for (const aux in this.goals) {
        this.metas.push(this.goals[aux].value);
        this.cantMedicion.push(aux);
        console.log(this.metas);
      }
    }
  }
  abiriModal(id) {
    for (const key in this.activities) {
      if (this.activities[key].id == id) {
      }
    }
  }
}
