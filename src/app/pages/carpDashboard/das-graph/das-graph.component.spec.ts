import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasGraphComponent } from './das-graph.component';

describe('DasGraphComponent', () => {
  let component: DasGraphComponent;
  let fixture: ComponentFixture<DasGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
