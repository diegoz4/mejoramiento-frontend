import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasIndicatorComponent } from './das-indicator.component';

describe('DasIndicatorComponent', () => {
  let component: DasIndicatorComponent;
  let fixture: ComponentFixture<DasIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
