import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
@Component({
  selector: "app-das-indicator",
  templateUrl: "./das-indicator.component.html",
  styleUrls: ["./das-indicator.component.css"],
})
export class DasIndicatorComponent implements OnInit {
  dataHeaders: any;
  idObjective: number;
  currentObjetive: any = { name: "" };
  forFilter: any;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idObjective = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "objective", value: this.idObjective };
  }

  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.currentObjetive = await this.dataService
      .getDataItem("objectives/", this.idObjective)
      .then((rs) => {
        return rs;
      });
    if (this.currentObjetive != null) {
      this.cambio = false;
    }
  }
  test(event) {
    this.router.navigate(["Graph", event.id]);
  }
}
