import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasObjectiveComponent } from './das-objective.component';

describe('DasObjectiveComponent', () => {
  let component: DasObjectiveComponent;
  let fixture: ComponentFixture<DasObjectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasObjectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasObjectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
