import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { async } from "rxjs/internal/scheduler/async";
import Swal from "sweetalert2";

import { DynamicTableComponent } from "src/app/components/dynamic-table/dynamic-table.component";
const swal = require("sweetalert2");
@Component({
  selector: "app-item-indicator",
  templateUrl: "./item-indicator.component.html",
  styleUrls: ["./item-indicator.component.css"],
})
export class ItemIndicatorComponent implements OnInit {
  @ViewChild("tbType", { static: true })
  dynamicTableFaculty: DynamicTableComponent;
  @ViewChild("tbPeridocidad", { static: true })
  dynamicTableProgram: DynamicTableComponent;

  dataTable: any;
  dataHeaders: any[];

  //periodicidad
  nombre: String;
  cantidad: number = 0;
  cambio: boolean = false;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    setTimeout(() => {
      this.cambio = false;
    }, 2000);
  }

  addItem() {
    Swal.mixin({
      input: "text",
      confirmButtonText: "Siguiente &rarr;",
      showCancelButton: true,
      progressSteps: ["1", "2"],
    })
      .queue([
        {
          title: "Periodicidad",
        },

        "Mensualidad",
      ])
      .then((result) => {
        if (result.value) {
          const answers = JSON.stringify(result.value);
          let json = {
            name: result.value[0],
            months: parseInt(result.value[1]),
          };

          this.dataService.addItem("periodicities/", json).subscribe((resp) => {
            this.updateDateTable();
          });
          Swal.fire({
            title: "Se Agrego correctamente la periodicidad",

            confirmButtonText: "OK!",
          });
        }
      });
  }
  addType() {
    swal
      .fire({
        title: "Digite el nombre del tipo de medición",
        input: "text",
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "Agregar",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        preConfirm: (name) => {
          return this.dataService
            .addItem("type_measurement/", { name })
            .subscribe((resp) => {
              this.updateDateTable();
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      })
      .then((result) => {
        if (result.id) {
          Swal.fire({
            title: `se agregó el tipo de medición Correctamente: ${result.value}`,
          });
        }
      });
  }
  editItem(event) {
    swal
      .fire({
        title: "Editar nombre de la facultad",
        input: "text",
        inputValue: event.name,
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "Agregar",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        preConfirm: (name) => {
          return this.dataService
            .editItem("type_measurement/", event.id, { name })
            .subscribe((resp) => {
              this.updateDateTable();
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      })
      .then((result) => {
        if (result.id) {
          Swal.fire({
            title: `se edito  la facultad Correctamente: ${result.value}`,
          });
        }
      });
  }
  editPeriodicidad(event) {
    Swal.mixin({
      input: "text",
      confirmButtonText: "Siguiente &rarr;",
      showCancelButton: true,
      progressSteps: ["1", "2"],
    })
      .queue([
        {
          title: "Periodicidad, " + event.name,
        },

        "Mensualidad, " + event.months,
      ])
      .then((result) => {
        if (result.value) {
          const answers = JSON.stringify(result.value);
          let json = {
            name: result.value[0],
            months: parseInt(result.value[1]),
          };

          this.dataService
            .editItem("periodicities/", event.id, json)
            .subscribe((resp) => {
              this.updateDateTable();
            });
          Swal.fire({
            title: "Se edito correctamente la periodicidad",

            confirmButtonText: "OK!",
          });
        }
      });
  }
  updateDateTable() {
    this.dynamicTableFaculty.getList();
    this.dynamicTableProgram.getList();
  }
}
