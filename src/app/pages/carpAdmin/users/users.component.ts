import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { async } from "rxjs/internal/scheduler/async";
import { MatDialog } from '@angular/material/dialog';
import { UniversalModalComponent } from 'src/app/components/universal-modal/universal-modal.component';

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  dataTable: any;
  dataHeadersUsers: any[];
  dataHeadersGroups: any[];

  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.dataHeadersUsers = [
      ['username','groups' , 'email', 'actions'],
      ['Nombre de usuario', 'Roles', 'Correo electrónico', 'Acciones']
    ];

    this.dataHeadersGroups = [
      ['name', 'actions'],
      ['Nombre', 'Acciones']
    ]
  }

  openDialogUsers(): void {
    const dialogRef = this.dialog.open(UniversalModalComponent, {
      width: '400px',
      data: {
        title: 'Agregar nuevo usuario',
        dataType: 'users'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)
    });
  }

  openDialogGroups(): void {
    const dialogRef = this.dialog.open(UniversalModalComponent, {
      width: '600px',
      data: {
        title: 'Agregar nuevo grupo',
        typeData: 'groups'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)
    });
  }
}
