import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "src/app/services/data.service";

import { DynamicTableComponent } from "src/app/components/dynamic-table/dynamic-table.component";

import Swal from "sweetalert2";

const swal = require("sweetalert2");
@Component({
  selector: "app-faculties",
  templateUrl: "./faculties.component.html",
  styleUrls: ["./faculties.component.css"],
})
export class FacultiesComponent implements OnInit {
  @ViewChild("tbFaculties", { static: false })
  dynamicTableFaculty: DynamicTableComponent;
  @ViewChild("tbPrograms", { static: false })
  dynamicTableProgram: DynamicTableComponent;
  dataTable: any;
  dataHeaders: any[];
  dataHeadersPrograms: any[];
  faculties: any = {};
  cambio: boolean = true;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.dataHeadersPrograms = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.dataService.getListItems("faculties/").then((rs) => {
      for (const iterator of Object.values(rs)) {
        this.faculties[iterator["id"]] = iterator.name;
      }
    });
    if (this.dataService != null) {
      setTimeout(() => {
        this.cambio = false;
      }, 2000);
    }
  }

  modalFacultad() {
    swal
      .fire({
        title: "Digite el nombre de la facultad",
        input: "text",
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "Agregar",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        preConfirm: (name) => {
          return this.dataService
            .addItem("faculties/", { name })
            .subscribe((resp) => {
              this.updateDateTable();
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      })
      .then((result) => {
        if (result.id) {
          Swal.fire({
            title: `se agregó la facultad Correctamente: ${result.value}`,
          });
        }
      });
  }
  editFaculty(event) {
    swal
      .fire({
        title: "Editar nombre de la facultad",
        input: "text",
        inputValue: event.name,
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "Agregar",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        preConfirm: (name) => {
          return this.dataService
            .editItem("faculties/", event.id, { name })
            .subscribe((resp) => {
              this.updateDateTable();
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      })
      .then((result) => {
        if (result.id) {
          Swal.fire({
            title: `se edito  la facultad Correctamente: ${result.value}`,
          });
        }
      });
  }

  editProgram(event) {
    swal
      .fire({
        title: "editar Programa",
        text: event.name,
        input: "text",
        inputValue: name,
        inputAttributes: {
          autocapitalize: "off",
        },
        showCancelButton: true,
        confirmButtonText: "Agregar",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        preConfirm: (name) => {
          let json = {
            id: event.id,
            name: name,
            faculty: event.faculty,
          };

          return this.dataService
            .editItem("programs", event.id, json)
            .subscribe((resp) => {
              this.updateDateTable();
            });
        },
        allowOutsideClick: () => !Swal.isLoading(),
      })
      .then((result) => {
        if (result.id) {
          Swal.fire({
            title: `se agregó la facultad Correctamente: ${result.value}`,
          });
        }
      });
  }

  async modalPrograma() {
    let prog = { name: "", faculty: null };
    const dta = await swal.fire({
      title: "Agregar Programa",
      input: "select",
      inputOptions: this.faculties,
      html:
        '<input id="nameprogram" class="swal2-input" placeholder="Ingrese Nombre del nuevo programa...">',
      inputPlaceholder: "Seleccione Facultad..",
      showCancelButton: true,
      inputValidator: (value) => {
        prog.faculty = value;
        return null;
      },
      preConfirm: () => {
        prog.name = document.getElementById("nameprogram")["value"];
      },
    });

    if (dta) {
      this.dataService.addItem("programs/", prog).subscribe((rs) => {
        this.updateDateTable();
      });
    }
  }

  updateDateTable() {
    this.dynamicTableFaculty.getList();
    this.dynamicTableProgram.getList();
  }
}
