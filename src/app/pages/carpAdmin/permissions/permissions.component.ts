import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit {
  dataHeadersPermissions: any

  constructor() { }

  ngOnInit() {
    this.dataHeadersPermissions = [
      ['name', 'codename', 'actions'],
      ['Nombre', 'Identificador','Acciones']
    ]
  }

}
