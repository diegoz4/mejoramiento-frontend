import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "app-report-plan",
  templateUrl: "./report-plan.component.html",
  styleUrls: ["./report-plan.component.css"],
})
export class ReportPlanComponent implements OnInit {
  dataTable: any;
  dataHeaders: any[];
  cambio: boolean = true;
  constructor(private router: Router) {}

  ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];
    setTimeout(() => {
      this.cambio = false;
    }, 2000);
  }
  test(event) {
    this.router.navigate(["reporObjective", event.id]);
  }
}
