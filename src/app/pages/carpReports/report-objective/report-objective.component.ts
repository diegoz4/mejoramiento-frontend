import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import Swal from "sweetalert2";
import * as html2pdf from "html2pdf.js";
import { Chart } from "chart.js";

const swal = require("sweetalert2");

@Component({
  selector: "app-report-objective",
  templateUrl: "./report-objective.component.html",
  styleUrls: ["./report-objective.component.css"],
})
export class ReportObjectiveComponent implements OnInit {
  report: any;
  @ViewChild("completo", { static: false }) completo: ElementRef;
  @ViewChild("personalizado", { static: false }) personalizado: ElementRef;

  currentPlan: any = { name: "" };
  idPlan: number;
  forFilter: any;
  chart: any;
  //todos los datos
  listObjetivo: any;
  listIndicators: any;
  indicators: any = [];
  listPeriodicidad: any;
  lisType: any;
  variables: any;
  goals: any;
  listActivity: any;
  listEstado: any;
  listMedicion: any;
  programs: any;
  facultad: any;
  states: any;

  //variables para construir el completo
  program: any = { name: "" };
  faculty: any = { name: "" };
  date: any;
  valor_calculado: any;
  indicadorFiltrado: any = [];

  //variables para cambiar de vista
  cambio: boolean = true;
  actualizar: boolean = false;
  mostrar: boolean = false;
  mostrarCompleto: boolean = false;
  OcultarSpinner: Boolean = false;
  //Variables para la grafica
  listMedicionGraf: any = [];
  cantMedicion: any = [];
  lineaBase: any = [];
  metas: any = [];

  //para persoalizado
  personal: boolean = false;
  currentObjetive: any = { name: "" };
  seleccionIndicator: any = [];

  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idPlan = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "improvement_plan", value: this.idPlan };
  }

  async ngOnInit() {
    this.currentPlan = await this.dataService
      .getDataItem("plans/", this.idPlan)
      .then((rs) => {
        return rs;
      });

    this.listObjetivo = Object.values(
      await this.dataService.getListItems("objectives/")
    ).filter((v) => v.improvement_plan == this.currentPlan.id);
    this.listObjetivo = Object.values(
      await this.dataService.getListItems("objectives/")
    ).filter((v) => v.improvement_plan == this.currentPlan.id);
    this.listIndicators = await this.dataService.getListItems("indicators/");
    this.listPeriodicidad = await this.dataService.getListItems(
      "periodicities/"
    );
    this.lisType = await this.dataService.getListItems("type_measurement/");
    this.variables = await this.dataService.getListItems("variables/");
    this.goals = await this.dataService.getListItems("goals/");
    this.listActivity = await this.dataService.getListItems("activities/");
    this.listEstado = await this.dataService.getListItems("states/");
    this.listMedicion = await this.dataService.getListItems("measurement/");
    this.programs = await this.dataService.getListItems("programs/");
    this.facultad = await this.dataService.getListItems("faculties/");
    this.states = await this.dataService.getListItems("states/");
    this.valor_calculado = await this.dataService.getListItems("calculated/");

    for (const i in this.listObjetivo) {
      for (const j in this.listIndicators) {
        if (this.listIndicators[j].objective == this.listObjetivo[i].id) {
          this.indicators.push(this.listIndicators[j]);
        }
      }
    }
    for (const key in this.programs) {
      if (this.programs[key].id == this.currentPlan.program) {
        this.program = this.programs[key];

        for (const a in this.facultad) {
          if (this.facultad[a].id == this.program.faculty) {
            this.faculty = this.facultad[a];
          }
        }
      }
    }
    var dat = new Date();
    this.date =
      dat.getDate() + "/" + (dat.getMonth() + 1) + "/" + dat.getFullYear();

    if (
      this.currentPlan == null ||
      this.listObjetivo == null ||
      this.listIndicators == null ||
      this.listIndicators == null ||
      this.indicators == null ||
      this.listPeriodicidad == null ||
      this.lisType == null ||
      this.variables == null ||
      this.goals == null ||
      this.listActivity == null ||
      this.listEstado == null ||
      this.listMedicion == null ||
      this.programs == null ||
      this.facultad == null ||
      this.states == null ||
      this.program == null ||
      this.faculty == null ||
      this.date == null ||
      this.valor_calculado == null ||
      this.listMedicionGraf == null ||
      this.cantMedicion == null ||
      this.lineaBase == null ||
      this.metas == null
    ) {
      this.cambio = true;
    } else {
      this.cambio = false;
    }
  }

  reporte() {
    if (this.report == "plan" || this.report == "personalizado") {
      this.seleccionIndicator = [];
      this.generarReporteCompleto();
    } else {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "No a seleccionado un tipo de reporte",
      });
    }
  }
  generarReporteCompleto() {
    if (this.mostrarCompleto) {
      var element = document.getElementById("completo");
      var opt = {
        margin: 0.5,
        filename: this.currentPlan.name,
        image: { type: "jpeg", quality: 1 },
        html2canvas: { scale: 3 },
        jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
      };

      html2pdf().from(element).set(opt).save();
    } else if (this.personal) {
      var element = document.getElementById("personalizado");
      var opt = {
        margin: 0.5,
        filename: this.currentPlan.name,
        image: { type: "jpeg", quality: 1 },
        html2canvas: { scale: 3 },
        jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
      };

      html2pdf().from(element).set(opt).save();
    }
  }

  filtroIndicador(item) {
    this.currentObjetive = {};

    this.actualizar = false;
    this.indicadorFiltrado = [];
    this.seleccionIndicator = [];
    for (let index = 0; index < this.listIndicators.length; index++) {
      if (this.listIndicators[index].objective == item) {
        this.indicadorFiltrado.push(this.listIndicators[index]);
      }
    }
    this.actualizar = true;
    for (let index = 0; index < this.listObjetivo.length; index++) {
      if (this.listObjetivo[index].id == item) {
        this.currentObjetive = this.listObjetivo[index];
      }
    }
  }
  checkValue(obj) {
    if (this.seleccionIndicator.length == 0) {
      this.seleccionIndicator.push(obj);
    } else {
      for (let index = 0; index < this.seleccionIndicator.length; index++) {
        if (this.seleccionIndicator[index].id == obj.id) {
          this.seleccionIndicator.splice(this.seleccionIndicator[index]);
        } else {
          this.seleccionIndicator.push(obj);
        }
      }
    }
  }

  activar() {
    this.personal = false;
    this.mostrarCompleto = true;
    this.mostrar = false;
    this.actualizar = false;
  }
  desactivar() {
    this.personal = true;
    this.mostrarCompleto = false;
    this.mostrar = true;
  }
  graficar(indicador) {
    this.listMedicionGraf = [];
    this.cantMedicion = [];
    this.lineaBase = [];
    this.metas = [];

    for (let key in this.valor_calculado) {
      if (this.valor_calculado[key].indicator == indicador.id) {
        this.listMedicionGraf.push(this.valor_calculado[key].value);

        this.lineaBase.push(indicador.base_line);
      }
    }

    for (const aux in this.goals) {
      if (this.goals[aux].indicator == indicador.id) {
        this.metas.push(this.goals[aux].value);
        this.cantMedicion.push(parseInt(aux) + 1);
      }
    }
    if (
      this.listMedicionGraf != null ||
      this.cantMedicion != null ||
      this.lineaBase != null ||
      this.metas != null
    ) {
      this.OcultarSpinner = false;
    }
  }
}
