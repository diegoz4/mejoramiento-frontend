import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportObjectiveComponent } from './report-objective.component';

describe('ReportObjectiveComponent', () => {
  let component: ReportObjectiveComponent;
  let fixture: ComponentFixture<ReportObjectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportObjectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportObjectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
