import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { PlansComponent } from "./infoPlan/plans/plans.component";
import { MaterialModule } from "../material.module";
import { ComponentsModule } from "../components/components.module";
import { FacultiesComponent } from "./carpAdmin/faculties/faculties.component";
import { ItemIndicatorComponent } from "./carpAdmin/item-indicator/item-indicator.component";
import { DashboardComponent } from "./carpDashboard/dashboard/dashboard.component";
import { UsersComponent } from "./carpAdmin/users/users.component";
import { DasObjectiveComponent } from "./carpDashboard/das-objective/das-objective.component";
import { DasIndicatorComponent } from "./carpDashboard/das-indicator/das-indicator.component";
import { DasGraphComponent } from "./carpDashboard/das-graph/das-graph.component";
import { ListPlanComponent } from "./carpActivities/list-plan/list-plan.component";
import { ListObjectiveComponent } from "./carpActivities/list-objective/list-objective.component";
import { ListIndicatorComponent } from "./carpActivities/list-indicator/list-indicator.component";
import { AddActivityComponent } from "./carpActivities/add-activity/add-activity.component";
import { LstPlansComponent } from "./carpMesumerent/lst-plans/lst-plans.component";
import { LstObjetiveComponent } from "./carpMesumerent/lst-objetive/lst-objetive.component";
import { LstIndicatorComponent } from "./carpMesumerent/lst-indicator/lst-indicator.component";
import { AddMesurementComponent } from "./carpMesumerent/add-mesurement/add-mesurement.component";
import { ObjectivesComponent } from "./infoPlan/objectives/objectives.component";
import { IndicatorsComponent } from "./infoPlan/indicators/indicators.component";
import { DescripIndicatorComponent } from "./infoPlan/descrip-indicator/descrip-indicator.component";
import { InfoActivitiesComponent } from "./carpActivities/info-activities/info-activities.component";
import { InfoActivityComponent } from "./carpDashboard/info-activity/info-activity.component";
import { ReportPlanComponent } from "./carpReports/report-plan/report-plan.component";
import { ReportObjectiveComponent } from "./carpReports/report-objective/report-objective.component";

import { ReactiveFormsModule } from "@angular/forms";
import { PermissionsComponent } from "./carpAdmin/permissions/permissions.component";

const ListModules = [
  DashboardComponent,
  PlansComponent,
  FacultiesComponent,
  ItemIndicatorComponent,
  UsersComponent,
  DasObjectiveComponent,
  DasIndicatorComponent,
  DasGraphComponent,
  ListPlanComponent,
  ListObjectiveComponent,
  ListIndicatorComponent,
  AddActivityComponent,
  LstPlansComponent,
  LstObjetiveComponent,
  LstIndicatorComponent,
  AddMesurementComponent,
  ObjectivesComponent,
  IndicatorsComponent,
  DescripIndicatorComponent,
  InfoActivitiesComponent,
  ReportPlanComponent,
  InfoActivityComponent,
  ReportObjectiveComponent,
  PermissionsComponent,
];

@NgModule({
  declarations: [ListModules],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule,
    MaterialModule,
  ],
  exports: [ComponentsModule, ListModules],
})
export class PagesModule {}
