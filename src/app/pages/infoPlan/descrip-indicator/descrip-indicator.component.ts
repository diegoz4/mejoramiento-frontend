import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import { DynamicTableComponent } from "src/app/components/dynamic-table/dynamic-table.component";

@Component({
  selector: "app-descrip-indicator",
  templateUrl: "./descrip-indicator.component.html",
  styleUrls: ["./descrip-indicator.component.css"],
})
export class DescripIndicatorComponent implements OnInit {
  @ViewChild("dtv", { static: false })
  dynamicTableVariables: DynamicTableComponent;
  @ViewChild("dtg", { static: false }) dynamicTableGoals: DynamicTableComponent;

  dataHeaders: any;
  dataHeadersGoals: any;
  idObjective: number;
  currentIndicator: any = { name: "" };

  forFilter: any;
  filterVariable: any = {};
  filterGoals: any = {};

  listaPeriodicidad: any;
  listTipoMedida: any;

  periodicidad: String;
  TypeMedicion: String;
  tendencia: String;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService
  ) {
    this.idObjective = this.activatedRouter.snapshot.params.id;
    this.forFilter = {
      filter: "objective",
      value: this.idObjective,
    };
    //  this.filterVariable = {'filter':'indicator', 'value': this.currentIndicator.id}

    this.dataService
      .getListItems("type_measurement/")
      .then((r) => (this.listTipoMedida = r));
    this.dataService
      .getListItems("periodicities/")
      .then((r) => (this.listaPeriodicidad = r));
  }
  async ngOnInit() {
    this.dataHeaders = [
      ["variable", "variable_name", "actions"],
      ["variable", "Nombre de la variable", "Acciones"],
    ];

    this.dataHeadersGoals = [
      ["goal_date", "value", "responsable", "actions"],
      ["Año", "valor", "Responsable", "Acciones"],
    ];
    this.currentIndicator = await this.dataService
      .getDataItem("indicators/", this.idObjective)
      .then((rs) => rs);
    // this.filterVariable = {'filter':'indicator', 'value': this.currentIndicator.id}

    if (this.currentIndicator) {
      this.filterVariable = {
        filter: "indicator",
        value: this.currentIndicator.id,
      };
      this.filterGoals = {
        filter: "indicator",
        value: this.currentIndicator.id,
      };

      // this.dynamicTableVariables.getList();
      // this.dynamicTableGoals.getList();
      this.cambio = false;
    }

    this.asignacion();
  }

  asignacion() {
    for (const index in this.listaPeriodicidad) {
      if (
        this.currentIndicator.periodicity == this.listaPeriodicidad[index].id
      ) {
        this.periodicidad = this.listaPeriodicidad[index].name;
      }
    }
    for (const key in this.listTipoMedida) {
      if (
        this.listTipoMedida[key].id == this.currentIndicator.typeMeasurement
      ) {
        this.TypeMedicion = this.listTipoMedida[key].name;
      }
    }
    if (this.currentIndicator.tendency == 2) {
      this.tendencia = "Subida";
    } else if (this.currentIndicator.tendency == 1) {
      this.tendencia = "Bajada";
    } else {
      this.tendencia = "Vacio";
    }
  }
}
