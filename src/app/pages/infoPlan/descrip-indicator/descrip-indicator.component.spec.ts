import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescripIndicatorComponent } from './descrip-indicator.component';

describe('DescripIndicatorComponent', () => {
  let component: DescripIndicatorComponent;
  let fixture: ComponentFixture<DescripIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescripIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescripIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
