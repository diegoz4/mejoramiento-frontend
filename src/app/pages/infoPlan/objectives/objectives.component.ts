import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-objectives",
  templateUrl: "./objectives.component.html",
  styleUrls: ["./objectives.component.css"],
})
export class ObjectivesComponent implements OnInit {
  dataHeaders: any;
  idPlan: number;
  currentPlan: any = { name: "" };
  forFilter: any;
  editar: boolean = false;
  programs: any;
  facultad: any;
  cambio: boolean = true;
  constructor(
    public dialog: MatDialog,
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idPlan = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "improvement_plan", value: this.idPlan };
  }

  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];
    this.programs = await this.dataService.getListItems("programs/");
    this.facultad = await this.dataService.getListItems("faculties/");

    this.currentPlan = await this.dataService
      .getDataItem("plans/", this.idPlan)
      .then((rs) => {
        return rs;
      });
    if (
      this.programs != null ||
      this.facultad != null ||
      this.currentPlan != null
    ) {
      this.cambio = false;
    }
  }
  test(event) {
    this.router.navigate(["infoIndicadores", event.id]);
  }
  habilitarEditado() {
    this.cambio = true;
    setInterval(() => {
      this.cambio = false;
    }, 100);

    this.editar = true;
  }
  GuardarEditado() {
    this.editar = false;
  }
  editarLoading() {}
}
