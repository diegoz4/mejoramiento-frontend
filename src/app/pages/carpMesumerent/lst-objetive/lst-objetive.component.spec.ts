import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LstObjetiveComponent } from './lst-objetive.component';

describe('LstObjetiveComponent', () => {
  let component: LstObjetiveComponent;
  let fixture: ComponentFixture<LstObjetiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LstObjetiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LstObjetiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
