import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
@Component({
  selector: "app-lst-objetive",
  templateUrl: "./lst-objetive.component.html",
  styleUrls: ["./lst-objetive.component.css"],
})
export class LstObjetiveComponent implements OnInit {
  dataHeaders: any;
  idPlan: number;
  currentPlan: any = { name: "" };
  forFilter: any;
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
    this.idPlan = this.activatedRouter.snapshot.params.id;
    this.forFilter = { filter: "improvement_plan", value: this.idPlan };
  }
  async ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    this.currentPlan = await this.dataService
      .getDataItem("plans/", this.idPlan)
      .then((rs) => {
        return rs;
      });
    if (this.dataHeaders == null && this.currentPlan == null) {
      this.cambio = true;
    } else {
      this.cambio = false;
    }
  }
  test(event) {
    this.router.navigate(["MedIndicator", event.id]);
  }
}
