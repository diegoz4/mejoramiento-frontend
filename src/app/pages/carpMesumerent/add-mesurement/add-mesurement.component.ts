import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import { FormGroup, FormControl } from "@angular/forms";
import { DynamicTableComponent } from "src/app/components/dynamic-table/dynamic-table.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DatePipe } from "@angular/common";

import Swal from "sweetalert2";
const swal = require("sweetalert2");

@Component({
  selector: "app-add-mesurement",
  templateUrl: "./add-mesurement.component.html",
  styleUrls: ["./add-mesurement.component.css"],
})
export class AddMesurementComponent implements OnInit {
  @ViewChild("dtm", { static: false })
  dynamicTableMeasurements: DynamicTableComponent;

  idIndicator: number;
  variables: any = [];
  currentIndicator: any = { name: "" };

  filterByIndicator: any;
  dataHeaders: any[];

  responsable: string;
  fecha: string;
  observacion: string;

  formulaIndicator: string;
  listVariables: any = [];

  formMeasurement: FormGroup;
  measurementAllOk: boolean = false;

  pipe = new DatePipe("en-US");
  cambio: boolean = true;
  constructor(
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private _snackBar: MatSnackBar
  ) {
    this.dataHeaders = [
      [
        "name",
        "value",
        "responsable",
        "measurement_date",
        "description",
        "actions",
      ],
      [
        "Nombre",
        "Valor",
        "Responsable",
        "Fecha medición",
        "Observaciones",
        "acciones",
      ],
    ];

    this.idIndicator = this.activatedRouter.snapshot.params.id;

    this.filterByIndicator = { filter: "indicator", value: this.idIndicator };

    this.formMeasurement = new FormGroup({
      responsable: new FormControl(),
      calculatedValue: new FormControl({ value: 0, disabled: true }),
      observations: new FormControl(),
    });
  }

  async ngOnInit() {
    this.currentIndicator = await this.dataService
      .getDataItem("indicators/", this.idIndicator)
      .then((rs) => rs);
    this.variables = Object.values(
      await this.dataService.getListItems("variables/").then((r) => r)
    ).filter((v) => v.indicator == this.currentIndicator.id);
    this.listVariables = [];
    this.formulaIndicator = this.currentIndicator.formula;

    if (this.currentIndicator != null || this.variables != null) {
      this.cambio = false;
    }
  }

  valor_calculado;

  setValues(v, event) {
    let variable = this.variables.find((e) => e.variable === v);

    this.formulaIndicator = this.formulaIndicator.replace(
      v,
      event.target.value
    );

    let dateee = new Date();

    let json = {
      name: variable.variable_name,
      value: event.target.value,
      responsable: this.formMeasurement["controls"]["responsable"].value,
      measurement_date: this.pipe.transform(new Date(), "yyyy-MM-dd"),
      description: "",
      indicator: this.idIndicator,
      variable: variable.id,
    };

    let vv = this.listVariables.find((e) => e.variable == variable.id);

    vv == undefined
      ? this.listVariables.push(json)
      : (this.listVariables[this.listVariables.indexOf(vv)] = json);

    console.log(this.variables);
    console.log(this.listVariables);

    this.listVariables.length == this.variables.length
      ? (this.measurementAllOk = this.checkValues())
      : null;

    var aux = 0;

    aux = eval(this.formulaIndicator);

    this.valor_calculado = aux.toFixed(2);

    this.formMeasurement["controls"]["calculatedValue"].setValue(
      this.valor_calculado
    );
  }

  checkValues() {
    let l = [];
    this.listVariables.forEach((e) =>
      e.value == "" || e.value == undefined ? l.push(e) : null
    );

    return l.length == 0;
  }

  saveMeasurement() {
    this.listVariables.forEach((e) => {
      e.description = this.formMeasurement["controls"]["observations"].value;
      console.log(e);

      this.dataService.addItem("measurement/", e).subscribe(() => {
        swal.fire("", "Se agrego Correctamente la medición", "success");

        this.dynamicTableMeasurements.getList();
      });
    });

    let jsonCalculated = {
      date_calculated: this.pipe.transform(new Date(), "yyyy-MM-dd"),
      value: this.formMeasurement["controls"]["calculatedValue"].value,
      indicator: this.currentIndicator.id,
    };

    this.dataService.addItem("calculated/", jsonCalculated).subscribe();

    // this.dataService.addItem('measurement/', json).subscribe(rs => {
    //   // this.updateDateTable()
    // })
  }
}
