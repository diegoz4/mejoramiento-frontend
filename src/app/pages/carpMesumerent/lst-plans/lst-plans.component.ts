import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { async } from "rxjs/internal/scheduler/async";
import { RouterLink, Router } from "@angular/router";
@Component({
  selector: "app-lst-plans",
  templateUrl: "./lst-plans.component.html",
  styleUrls: ["./lst-plans.component.css"],
})
export class LstPlansComponent implements OnInit {
  dataTable: any;
  dataHeaders: any[];
  cambio: boolean = true;
  constructor(private router: Router) {}

  ngOnInit() {
    this.dataHeaders = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];
    if (this.dataHeaders != null) {
      setTimeout(() => {
        this.cambio = false;
      }, 2000);
    }
  }
  test(event) {
    this.router.navigate(["MedObjective", event.id]);
  }
}
