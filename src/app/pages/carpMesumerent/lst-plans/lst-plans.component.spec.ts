import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LstPlansComponent } from './lst-plans.component';

describe('LstPlansComponent', () => {
  let component: LstPlansComponent;
  let fixture: ComponentFixture<LstPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LstPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LstPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
