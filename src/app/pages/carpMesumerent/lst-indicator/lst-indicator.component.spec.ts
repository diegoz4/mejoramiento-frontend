import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LstIndicatorComponent } from './lst-indicator.component';

describe('LstIndicatorComponent', () => {
  let component: LstIndicatorComponent;
  let fixture: ComponentFixture<LstIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LstIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LstIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
