import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DataService {
  token: string;
  urlApi: string;
  headers: HttpHeaders;

  constructor(
    private _httpClient: HttpClient,
    private _authService: AuthService
  ) {
    this.urlApi = "http://diegoz4.pythonanywhere.com/api/v1.0";
    this.headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "JWT " + this._authService.token,
    });
  }

  getListItems(rute: string) {
    return this._httpClient.get(`${this.urlApi}/${rute}`).toPromise();
  }

  addItem(rute: string, params: object) {
    return this._httpClient.post(
      `${this.urlApi}/${rute}`,
      JSON.stringify(params),
      { headers: this.headers }
    );
  }

  removeItem(rute: string, id: any) {
    return this._httpClient.delete(`${this.urlApi}/${rute}/${id}/`, {
      headers: this.headers,
    });
  }

  editItem(rute: string, id: any, params: object) {
    return this._httpClient.put(
      `${this.urlApi}/${rute}/${id}/`,
      JSON.stringify(params),
      { headers: this.headers }
    );
  }

  getDataItem(rute: string, id: any) {
    return this._httpClient.get(`${this.urlApi}/${rute}/${id}`).toPromise();
  }
}
