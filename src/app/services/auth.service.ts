import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  urlApi: string
  headers: HttpHeaders
  params: HttpParams
  token: string
  token_expires: Date
  userName: string
  errors: any = []

  constructor(private _http: HttpClient) {
    this.urlApi = 'https://diegoz4.pythonanywhere.com/api/v1.0'
    this.headers = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
  }

  async login(user) {
    this.params = new HttpParams({ fromObject: user })

    this._http.post(`${this.urlApi}/token/`, this.params, { headers: this.headers }).subscribe(
      data => {
        this.updateData(data['token']);
        localStorage.setItem('heros', data['token'])
      },
      err => {
        this.errors = err['error'];
        this.logout()
      }
    )
  }

  refreshToken() {
    this._http.post(`${this.urlApi}/token/refresh`, JSON.stringify({ token: this.token }), { headers: this.headers }).subscribe(
      data => {
        this.updateData(data['token']);
      },
      err => {
        this.errors = err['error'];
      }
    )
  }

  logout() {
    this.token = null;
    this.token_expires = null;
    this.userName = null;
    localStorage.removeItem('heros')
  }

  updateData(token: string) {
    this.token = token;
    this.errors = [];

    let token_parts = this.token.split(/\./);
    let token_decoded = JSON.parse(window.atob(token_parts[1]))
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.userName = token_decoded.username;
  }
}
