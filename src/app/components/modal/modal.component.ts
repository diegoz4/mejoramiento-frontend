import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "src/app/services/data.service";

import * as $ from "jquery";
import { FormControl, FormGroup } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DynamicTableComponent } from "../dynamic-table/dynamic-table.component";
const jQuery = require("jquery.easing");

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"],
})
export class ModalComponent implements OnInit {
  // @ViewChild('tbPlans', { static: true }) dynamicTablePlans: DynamicTableComponent
  @ViewChild("tbObjectives", { static: false })
  dynamicTableObjectives: DynamicTableComponent;
  @ViewChild("tbGoals", { static: false })
  dynamicTableGoals: DynamicTableComponent;
  @ViewChild("tbFormula", { static: false })
  dynamicTableFormula: DynamicTableComponent;

  // Plan
  currentImprovementPlan: any = { id: null };

  // Objectives
  currentObjective: any = { id: null };
  dataHeadersObjectives: any[];
  forFilterObjectives: any;

  // Indicator
  currentIndicator: any = { id: null };

  // Goals
  dataHeadersGoals: any[];
  currentGoals: any[] = [];
  forFilterGoals: any;
  inndexGoal: number = 0;

  // Variables
  currentsVariables: any[] = [];

  faculties: {};
  programs: {};
  measurements: {};
  periodicities: {};

  filteredPrograms: {};

  formPlan: FormGroup;
  formObjective: FormGroup;
  formIndicator: FormGroup;
  formGoal: FormGroup;
  formVariables: FormGroup;

  maxDate: Date;
  minDate: Date;

  currentField;
  nextField;
  previousField;
  left;
  opacity;
  scale;
  animating;

  constructor(
    private _dataService: DataService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._dataService
      .getListItems("faculties/")
      .then((r) => (this.faculties = r));
    this._dataService
      .getListItems("programs/")
      .then((r) => (this.programs = r));
    this._dataService
      .getListItems("type_measurement/")
      .then((r) => (this.measurements = r));
    this._dataService
      .getListItems("periodicities/")
      .then((r) => (this.periodicities = r));

    // Plan
    this.formPlan = new FormGroup({
      selectFaculty: new FormControl(),
      selectProgram: new FormControl(),
      namePlan: new FormControl(),
      startDate: new FormControl(),
      endDate: new FormControl(),
      observations: new FormControl("Sin observaciones"),
    });

    // Objectives
    this.formObjective = new FormGroup({
      nameObjective: new FormControl(),
      observationObjective: new FormControl("Sin observaciones"),
    });

    this.dataHeadersObjectives = [
      ["name", "actions"],
      ["Nombre", "Acciones"],
    ];

    // Indicator
    this.formIndicator = new FormGroup({
      nameIndicator: new FormControl(),
      typeMeasurement: new FormControl(),
      base_line: new FormControl(),
      tendency: new FormControl(false),
      periodicity: new FormControl(),
      responsable: new FormControl(),
      observations: new FormControl("Sin observaciones"),
      formula: new FormControl(),
      description_formula: new FormControl(),
    });

    // Goals
    this.formGoal = new FormGroup({
      anio: new FormControl(),
      valueGoal: new FormControl(),
      responsable: new FormControl(),
    });
    this.dataHeadersGoals = [
      ["value", "goal_date", "responsable", "actions"],
      ["Valor meta", "Fecha", "Responsable", "Acciones"],
    ];

    // Variables
    this.formVariables = new FormGroup({
      name: new FormControl(),
      variable: new FormControl(String.fromCharCode(this.initialVariable)),
    });

    this.formPlan.get("startDate").setValue(new Date());
    this.setThreeYears();
  }

  getPrograms(faculty) {
    this.filteredPrograms = Object.values(this.programs).filter(
      (p) => p["faculty"] == faculty
    );
  }

  setThreeYears() {
    let sdt = this.formPlan.get("startDate").value;
    let dt = new Date(sdt);
    this.minDate = new Date(sdt);
    this.maxDate = new Date(dt.setFullYear(dt.getFullYear() + 3));
    this.formPlan.get("endDate").setValue(this.maxDate);
  }

  savePlan() {
    let json = {
      name: this.formPlan.value["namePlan"],
      start_date: this.formPlan.value["startDate"].toISOString().slice(0, 10),
      end_date: this.formPlan.value["endDate"].toISOString().slice(0, 10),
      description: this.formPlan.value["description"],
      program: this.formPlan.value["selectProgram"],
      autor: 1,
    };

    this._dataService.addItem("plans/", json).subscribe((resp) => {
      this.currentImprovementPlan = resp;
      this.forFilterObjectives = {
        filter: "currentImprovementPlan",
        value: this.currentImprovementPlan["id"],
      };
      this.formPlan.reset();
      this._snackBar.open(
        "¡Se registró con éxito el plan de mejoramiento!",
        "Aceptar",
        {
          duration: 3000,
        }
      );
      // this.dynamicTablePlans.getList()
      this.dynamicTableObjectives.getList();
    });
  }

  saveObjective() {
    let json = {
      name: this.formObjective.value["nameObjective"],
      observations: this.formObjective.value["observationObjective"],
      improvement_plan: this.currentImprovementPlan["id"],
      autor: 1,
    };

    this._dataService.addItem("objectives/", json).subscribe((res) => {
      this._snackBar.open("¡Se registró con éxito el objetivo!", "Aceptar", {
        duration: 3000,
      });

      this.formObjective["controls"]["nameObjective"].reset();
      this.formObjective["controls"]["observationObjective"].setValue(
        "Sin observaciones"
      );
      this.dynamicTableObjectives.getList();
    });
  }

  saveIndicator(event?) {
    let jsonIndicator = {
      name: this.formIndicator.value["nameIndicator"],
      typeMeasurement: this.formIndicator.value["typeMeasurement"],
      base_line: this.formIndicator.value["base_line"],
      tendency: this.formIndicator.value["tendency"] ? 2 : 1,
      periodicity: this.formIndicator.value["periodicity"],
      responsable: this.formIndicator.value["responsable"],
      observations: this.formIndicator.value["observations"],
      formula: this.formIndicator.value["formula"],
      description_formula: this.formIndicator.value["description_formula"],
      objective: this.currentObjective["id"],
      autor: 1,
    };

    let allOk = false;

    let jsonGoals = [];
    let jsonVariables = [];

    this._dataService.addItem("indicators/", jsonIndicator).subscribe((res) => {
      this.currentGoals.map((c) => {
        jsonGoals.push({
          goal_date: c.goal_date,
          value: c.value,
          responsable: c.responsable,
          indicator: res["id"],
        });
      });

      this.currentsVariables.map((v) => {
        jsonVariables.push({
          variable_name: v.variable_name,
          variable: v.variable,
          indicator: res["id"],
        });
      });

      // Send data to api

      jsonGoals.map((g) => {
        this._dataService.addItem("goals/", g).subscribe(
          (rs) => {
            allOk = true;
          },
          (error) => {
            allOk = false;
          }
        );
      });

      jsonVariables.map((v) => {
        this._dataService.addItem("variables/", v).subscribe(
          (rs) => {
            allOk = true;
          },
          (error) => {
            allOk = false;
          }
        );
      });

      this.formIndicator.reset();
      this.currentGoals = [];
    });
  }

  setIndicator(event) {
    this.setNextField($(document.getElementsByTagName("fieldset")[0].closest("fieldset")).next(), true);

    this.currentObjective = event;
  }

  setGoal() {
    this.currentGoals.length != 0
      ? (this.inndexGoal += 1)
      : (this.inndexGoal = 0);

    let json = {
      id: this.inndexGoal,
      goal_date: this.formGoal.value["anio"],
      value: this.formGoal.value["valueGoal"],
      responsable: this.formGoal.value["responsable"],
    };

    this.currentGoals.push(json);
    this.dynamicTableGoals.getList();

    this.formGoal.reset();
  }

  initialVariable: number = 65;

  setVariable() {
    let json = {
      variable_name: this.formVariables.value["name"],
      variable: this.formVariables.value["variable"],
    };

    let valFormula =
      this.formIndicator.value["formula"] == null
        ? ""
        : this.formIndicator.value["formula"];
    valFormula += this.formVariables.value["variable"];

    this.formIndicator["controls"]["formula"].setValue(valFormula);
    this.formVariables.reset();

    this.initialVariable += 1;
    this.formVariables["controls"]["variable"].setValue(
      String.fromCharCode(this.initialVariable)
    );
    this.currentsVariables.push(json);
  }

  closeDialog() {
    let modal_t = document.getElementById("modal_1");
    modal_t.classList.remove("sshow");
    modal_t.classList.add("hhidden");
  }

  setNextField(event, exist: boolean) {
    // if (this.animating) return false;
    // this.animating = true;

    this.currentField = exist ? event : $(event.target.closest("fieldset"));
    this.nextField = this.currentField.next();

    var current_fs = this.currentField;
    var next_fs = this.nextField;

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li")
      .eq($("fieldset").index(this.nextField))
      .addClass("active");

    //show the next fieldset
    this.nextField.show();

    //hide the current fieldset with style
    this.currentField.animate(
      { opacity: 0 },
      {
        step: function (now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale current_fs down to 80%
          this.scale = 1 - (1 - now) * 0.2;
          //2. bring next_fs from the right(50%)
          this.left = now * 50 + "%";
          //3. increase opacity of next_fs to 1 as it moves in
          this.opacity = 1 - now;

          current_fs.css({
            transform: "scale(" + this.scale + ")",
            position: "absolute",
          });
          next_fs.css({ left: this.left, opacity: this.opacity });
        },
        duration: 800,
        complete: function () {
          current_fs.hide();
          this.animating = false;
        },
        //this comes from the custom easing plugin
        easing: "easeInOutBack",
      }
    );

    this.animating = false;
  }

  setPreviousField(event) {
    // if (this.animating) return false;
    // this.animating = true;

    this.currentField = $(event.target.closest("fieldset"));
    this.previousField = this.currentField.prev();

    var current_fs = this.currentField;
    var previous_fs = this.previousField;

    //de-activate current step on progressbar
    $("#progressbar li")
      .eq($("fieldset").index(this.currentField))
      .removeClass("active");

    //show the previous fieldset
    this.previousField.show();
    //hide the current fieldset with style
    this.currentField.animate(
      { opacity: 0 },
      {
        step: function (now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale previous_fs from 80% to 100%
          this.scale = 0.8 + (1 - now) * 0.2;
          //2. take current_fs to the right(50%) - from 0%
          this.left = (1 - now) * 50 + "%";
          //3. increase opacity of previous_fs to 1 as it moves in
          this.opacity = 1 - now;
          current_fs.css({ left: this.left });
          previous_fs.css({
            transform: "scale(" + this.scale + ")",
            opacity: this.opacity,
          });
        },
        duration: 800,
        complete: function () {
          current_fs.hide();
          this.animating = false;
        },

        easing: "easeInOutBack",
      }
    );

    this.animating = false;
  }
}
