import { Component, OnInit, Input } from "@angular/core";
import { Chart } from "chart.js";
@Component({
  selector: "app-graph-line",
  templateUrl: "./graph-line.component.html",
  styleUrls: ["./graph-line.component.css"],
})
export class GraphLineComponent implements OnInit {
  @Input() listMediciones: any[];
  @Input() cantiMediciones: any[];
  @Input() lineaBase: any[];
  @Input() metas: any[];
  chart: any;

  constructor() {}

  async ngOnInit() {
    this.chart = new Chart("canvas", {
      type: "line",
      data: {
        labels: this.cantiMediciones,
        datasets: [
          {
            //primer dato.
            label: "Linea Base",
            data: this.lineaBase,

            borderWidth: 2,
            backgroundColor: [
              "rgba(255, 0,0, 0.5)",
              "rgba(255, 0, 0, 0.25)",
              "rgba(255, 0, 0, 0)",
            ],
            borderColor: "#FC2525",
            fill: false,
          },
          {
            //segudno dato
            label: "Mediciones",
            backgroundColor: ["rgba(41, 179, 181, 1)"],
            borderWidth: 3,
            borderColor: ["rgba(41, 179, 181, 1)"],
            data: this.listMediciones,
            fill: false,
          },
          {
            //tercer dato.
            label: "Metas",
            data: this.metas,

            borderWidth: 2,
            backgroundColor: ["rgba(215, 74, 239, 1)"],
            borderColor: "#BF3FBF",
            fill: false,
          },
        ],
      },
      responsive: true,
      maintainAspectRatio: false,
      options: {
        legend: {
          display: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Cantidad de mediciones",
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "valor de la medición",
              },
            },
          ],
        },
      },
    });
  }
}
