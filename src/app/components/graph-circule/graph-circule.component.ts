import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-graph-circule',
  templateUrl: './graph-circule.component.html',
  styleUrls: ['./graph-circule.component.css']
})
export class GraphCirculeComponent implements OnInit {
  chart: any; 
    @Input () noInicia:number
    @Input () cancelada:number
    @Input () enCurso:number
    @Input () ejecutada:number
    @Input () Finalizada:number
    constructor() { }

  ngOnInit() {
    
    this.chart = new Chart('canva', {
      type: 'doughnut',
      data: {
        labels: ['No inicia','Cancelada','En curso','Ejecutada','Finalizada'],
        datasets: [
          { 
            data: [this.noInicia,this.cancelada,this.enCurso,this.ejecutada,this.Finalizada],
            backgroundColor: ['rgba(255, 0, 0, 0.42)','rgba(255, 169, 20, 0.84)','rgba(20, 196, 255, 1)','rgba(232, 20, 255,0.98)','rgba(20, 255, 106, 0.84)'],
            fill: true
          },
        ]
      },
      
      options: {
        legend: {
          display: true
        },
        tooltips:{
          enabled:true
        }
      }
    });
  }

}
