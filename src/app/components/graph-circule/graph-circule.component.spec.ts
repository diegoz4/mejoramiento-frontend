import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphCirculeComponent } from './graph-circule.component';

describe('GraphCirculeComponent', () => {
  let component: GraphCirculeComponent;
  let fixture: ComponentFixture<GraphCirculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphCirculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphCirculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
