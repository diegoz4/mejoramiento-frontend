import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { SelectionModel } from "@angular/cdk/collections";
import { DataService } from "src/app/services/data.service";
import { MatDialogRef, MatDialog } from "@angular/material/dialog";
import { ModalComponent } from "../modal/modal.component";
import Swal from "sweetalert2";

const swal = require("sweetalert2");

@Component({
  selector: "app-dynamic-table",
  templateUrl: "./dynamic-table.component.html",
  styleUrls: ["./dynamic-table.component.css"],
})
export class DynamicTableComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("tb", { static: true }) dynamicTable: MatTable<any>;

  @Input() staticData: any;
  @Input() pathData: string;
  @Input() filterData: any;
  @Input() dataHeaders: any[];
  @Input() add: boolean = true;

  @Input() search: boolean = true;
  @Input() informative: boolean = true;
  @Input() detailed: boolean = true;
  @Input() editable: boolean = false;
  @Input() removable: boolean = false;

  @Output() viewData = new EventEmitter<any>();

  dataRefColumns: string[];
  displayedColumns: string[];

  dataSource: MatTableDataSource<object>;
  selection = new SelectionModel<any>(true, []);

  constructor(private _dataService: DataService, public _dialog: MatDialog) {}

  ngOnInit() {
    this.dataRefColumns = this.dataHeaders[0];
    this.displayedColumns = this.dataHeaders[1];

    this.getList();
  }

  public getList() {
    if (this.staticData != undefined) {
      this.dataSource = new MatTableDataSource<object>(this.staticData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dynamicTable.renderRows();
    } else {
      this._dataService.getListItems(this.pathData).then(async (rs) => {
        if (this.filterData) {
          this.dataSource = new MatTableDataSource<object>(
            Object.values(rs).filter(
              (e) =>
                e.improvement_plan == this.filterData.value ||
                e.objective == this.filterData.value ||
                e.indicator == this.filterData.value
            )
          );
        } else {
          this.dataSource = new MatTableDataSource<object>(Object.values(rs));
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }
  }

  addRow() {
    let modal_t = document.getElementById("modal_1");
    modal_t.classList.remove("hhidden");
    modal_t.classList.add("sshow");
  }

  removeRow(row) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Eliminar!",
        text: "Esta seguro de eliminar este elemento",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.value) {
          if (this.staticData != undefined) {
            let rowDel = this.staticData.indexOf(row);
            this.staticData.splice(rowDel, 1);
            this.dynamicTable.renderRows();
          } else {
            this._dataService
              .removeItem(this.pathData, row.id)
              .subscribe(() => {
                this.getList();
              });
          }

          swalWithBootstrapButtons.fire(
            "Eliminado",
            "Se ha eliminado correctamente este elemento",
            "success"
          );
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelado",
            "No sera eliminado este elemento :)",
            "error"
          );
        }
      });
  }

  viewRow(row) {
    this.viewData.emit(row);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${
      row.position + 1
    }`;
  }
}
