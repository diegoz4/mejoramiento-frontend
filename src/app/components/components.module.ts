import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

import { MaterialModule } from "../material.module";

import { SidebarComponent } from "./sidebar/sidebar.component";
import { CardTemplateComponent } from "./card-template/card-template.component";
import { DynamicTableComponent } from "./dynamic-table/dynamic-table.component";
import { ProgressBarComponent } from "./progress-bar/progress-bar.component";
import { ModalComponent } from "./modal/modal.component";
import { GraphLineComponent } from "./graph-line/graph-line.component";
import { GraphCirculeComponent } from "./graph-circule/graph-circule.component";
import { BreadcrumbsComponent } from "./breadcrumbs/breadcrumbs.component";

import { UniversalModalComponent } from "./universal-modal/universal-modal.component";
import { LoadingComponent } from "./loading/loading.component";

const ListComponents = [
  SidebarComponent,
  CardTemplateComponent,
  DynamicTableComponent,
  ProgressBarComponent,
  ModalComponent,
  GraphLineComponent,
  GraphCirculeComponent,
  BreadcrumbsComponent,
  LoadingComponent,
  UniversalModalComponent,
];
@NgModule({
  declarations: [ListComponents],
  imports: [
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    RouterModule,
    FormsModule,
  ],
  entryComponents: [UniversalModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [ListComponents],
})
export class ComponentsModule {}
