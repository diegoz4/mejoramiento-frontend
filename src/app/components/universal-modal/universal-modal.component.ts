import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-universal-modal',
  templateUrl: './universal-modal.component.html',
  styleUrls: ['./universal-modal.component.css']
})
export class UniversalModalComponent implements OnInit {
  formGroups: FormGroup
  permissionsList: any

  constructor(
    private _dataService: DataService,
    public dialogRef: MatDialogRef<UniversalModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.formGroups = new FormGroup({
      nameGroup: new FormControl()
    })

    this.getPermissions()
  }

  async getPermissions() {
    let list
    list = await this._dataService.getListItems('permissions/').then(rs => {
      console.log(rs)
      return rs
    })

    this.permissionsList = Object.values(list.reduce((r, a) => {
      r[a.content_type] = [...r[a.content_type] || [], a];
      return r;
    }, {}))

    this.permissionsList.forEach(e => {
      let aux = e[0].name.split(' ')
      aux.shift()
      e[0].name = aux.join(' ')
      e[0].name = e[0].name.charAt(0).toUpperCase() + e[0].name.slice(1).toLowerCase()
    });

    console.log(this.permissionsList)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
