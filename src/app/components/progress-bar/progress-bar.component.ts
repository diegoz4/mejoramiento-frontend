import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styles: [`
              .progress-value {
                position: absolute;
                right: 15%;
                z-index: 102;
              }
  
              mat-progress-bar {
                border-radius: 8px;
              }
          `
  ]
})
export class ProgressBarComponent implements OnInit {
  @Input() value: number
  @Input() color: string
  @Input() mode: string
  @Input() height: number

  constructor() { }

  ngOnInit() {
  }

}
